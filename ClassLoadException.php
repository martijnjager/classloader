<?php
namespace Bootstrap;

use Exception;

class ClassLoadException extends Exception
{
    public function __construct($file)
    {
        $this->exceptionFileLoading($file);
    }


    /**
     * Get error message if GetClassDirectory fails
     * This function should only be called to retrieve the error and after CheckClassDirectory has been called
     *
     * @param string $dir
     *
     * @return string Error message
     */
    private function exceptionFileLoading($dir)
    {
        echo "ERROR, class located $dir is not in the correct directory, not readable or non-existing.";
    }
}