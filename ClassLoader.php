<?php
/**
 *
 * This class is meant to autoload all existing classes for this project
 * It should check if the by user given classes are located in the by user given directory
 * - If this is the case then it should include the classes
 * - If this is not the case, then show an error message that a class is missing
 *
 *
 * changelog:
 *    13-jan-2017
 *        Fixed issue where CheckClassDirectory and GetClassErrorMessage were calling each other in an endless loop
 *        Added check for valid directory for error message
 *
 *    14-jan-2017
 *        Removed array compatibility due to incompatibility with input from outsides
 *
 *    20-jan-2017
 *        Changed the functionality of this class. Only GetListClass needs to be called to include all predefined classess
 *        Added array compatibility for class names due the change how the class works
 *
 *    23-jan-2017
 *        Improved algorithm for searching for file if it is not in the provided directory
 *
 */

/**
 * LoadClass
 *
 * @package DynamicLoadClass
 * @author Martijn Jager <martijnjager@live.nl>
 * @version 1.0
 * @access private
 */

namespace Bootstrap;

use Exception;

class ClassLoader
{
    protected $files;
    protected $dir;

    public function __construct($dir = __DIR__)
    {
        $this->dir = $dir;
    }

    public function loadClasses(array $classes = null)
    {
        $this->checkDirectory($this->dir);

        $this->getClasses($classes);
    }

    /**
     * Make sure the used directory is a valid one
     *
     * @param $dir
     */
    public function checkDirectory($dir){
        while(file_exists($dir) !== true){
            $dir = substr($dir, 0, strripos($dir, '/'));
        }

        $this->dir = $dir;
    }

    /**
     * Put all classes in property
     * @param $classes
     * @return string
     * @throws Exception
     */
    public function getClasses($classes)
    {
        if (is_array($classes))
        {
            foreach ($classes as $class) {
                $class = $this->validateExtension($class);

                if(!is_readable($class))
                    throw new Exception("$class is not readable");

                $this->files[] = $this->dir.$class;
            }
        }else{

            foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($this->dir)) as $file){
                if(is_file($file) && is_readable($file) && !strripos($file, '.idea')){
                    $this->files[] = $file->getPathName();
                }
            }
        }

        $this->loadEnvironment();
    }

    /**
     * Make sure the file input has an extension
     * @param string $file
     * @param string $extension
     * @return string
     */
    public function validateExtension($file, $extension = '.php')
    {
        return strripos($file, $extension) ? $file : $file.$extension;
    }


    /**
     * Include file
     *
     * @param string $dir Directory that contains a class needed to be included
     *
     * @return bool true on succes, false on failure
     */
    private function load()
    {
        foreach($this->files as $file){
            include_once $file;
        }
    }

    protected function loadFilesInEnv()
    {
        setenv('files', $this->files);
    }



    protected function loadEnvironment()
    {
        $this->load();

        $this->loadFilesInEnv();

        $loader = new Loader($this->dir.'\.env');
        $loader->loadEnvironment();
    }
}